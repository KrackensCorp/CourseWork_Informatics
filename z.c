//	S - множетсво входных функциональных зависимостей(ФЗ),
//	F - множество ФЗ, которые проверяются на выводимость.
//	Каждая ФЗ из S и F представляется си-строкой, для возможности использования функций
//	библиотеки string.h. 
#define _CRT_SECURE_NO_WARNINGS	// при сообщениях компилятора о небезопасных функциях, раскомментировать

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define inputf "Input.txt"							//	входной файл
#define outputf "Output.txt"						//  выходной файл
#define A_COUNT 26									//  количество возможных атрибутов
#define MAX_FD_LENGTH   A_COUNT * 2 + 2 + 1 + 1		//  максимальная возможная длина всей строки,
													//		представляющей ФЗ: учитываются символы
													//			'-'  ,  '>'  ,  '\n'  ,  '\0'
void DoWork();
int CheckForDeductibilty(char*, char**, int);
char * AddAttribute(char *, char);
int DoesLeftOfFDBelongToX(char *, char *);
char * AddRightOfFDToX(char *, char *);
int DoesRightOfFDBelongToX(char *, char *);
FILE *fin, *fout;

int main()
{
	if ((fin = fopen(inputf, "r")) == NULL)
	{
		perror(inputf);
		getchar();
		return 1;
	}
	if ((fout = fopen(outputf, "w")) == NULL)
	{
		perror(outputf);
		getchar();
		return 2;
	}
	// вызов основной функции
	DoWork();

	puts("Go?");
	getchar();
	fclose(fout);
	fclose(fin);
	return 0;
}
void DoWork()
{
	// ФЗ из S будут храниться в динамическом массиве си-строк

	// указатель на массив
	char **S_fd = NULL;
	// кол-во ФЗ из S
	int S_cnt = 0;

	char str[MAX_FD_LENGTH];
	char *ch;

	// создаем и заполняем массив построчным считыванием входного файла
	while (fgets(str, MAX_FD_LENGTH, fin) != NULL)
	{
		// если наткнулись на обозначения блока ФЗ из F, выходим из цикла
		if (strstr(str, "GOAL:") != NULL)
			break;
		// пропускаем ненужные строки
		if (strstr(str, "->") == NULL)
			continue;
		// убираем в строке символ перехода на новую строку
		if ((ch = strchr(str, '\n')) != NULL)
			*ch = '\0';

		S_cnt++;
		S_fd = (char **)realloc(S_fd, sizeof(char *) * S_cnt);
		S_fd[S_cnt - 1] = (char *)malloc(sizeof(str));
		strcpy(S_fd[S_cnt-1], str);
	}
	// считываем ФЗ из F и сразу запускаем процедуру установления выводимости
	while (fgets(str, MAX_FD_LENGTH, fin) != NULL)
	{
		if (strstr(str, "->") == NULL)
			continue;
		if ((ch = strchr(str, '\n')) != NULL)
			*ch = '\0';

		fputs(str, fout);

		if (CheckForDeductibilty(str, S_fd, S_cnt) == 1)
			fputs(" : YES\n", fout);
		else
			fputs(" : NO\n", fout);
	}
// освобождаем память
	for (; S_cnt > 0; S_cnt--)
		free(S_fd[S_cnt - 1]);

	free(S_fd);
}
int CheckForDeductibilty(char *fd, char **s_fd, int s_fd_cnt)
{
	// Процедура построения замыкания множества атрибутов X левой части тестируемой ФЗ
	// и определения, входит ли ее правая часть в это замыкание.
	// fd - тестируемая ФЗ
	// s_fd - ФЗ-и из S
	// s_fd_cnt - кол-во ФЗ в S
	int i, j, rezult;
	int added;			// флаг, символизирующий, что на очередном шаге алгоритма в замыкание X был добавлен
						// хотя бы один атрибут
	// полагаем замыканием X всю левую часть ФЗ fd
	char *X = NULL;
	for (i = 0; fd[i] != '-'; i++)
		X = AddAttribute(X, fd[i]);

	// добавляем в X новые атрибуты
	do
	{
		added = 0;

		for (i = 0; i < s_fd_cnt; i++)				// все ФЗ из S
			if (DoesLeftOfFDBelongToX(s_fd[i], X))	// если левая часть ФЗ s_fd[i] принадлежит текущему замыканию X,
			{
				j = strlen(X);
				X = AddRightOfFDToX(s_fd[i], X);	// добавляем аттрибуты из правой части ФЗ s_fd[i]
				if ((int)strlen(X) > j)
					added = 1;						// устанавливаем флаг, что алгоритм построения замыкания не закончен
			}
	} while (added);
	
	// Замыкание X построено, проверяем принадлежит ли ему правая часть тестовой ФЗ
	rezult = DoesRightOfFDBelongToX(fd, X);

	free(X);
	return rezult;
}
int DoesLeftOfFDBelongToX(char *fd, char *x)
{
	char *ch;
	for (ch = fd; *ch != '-'; ch++)
		if (!strchr(x, *ch))		// если хотя бы один атрибут не входит в X,
			return 0;				// вовращаем "нет"
	return 1;
}
int DoesRightOfFDBelongToX(char *fd, char *x)
{
	int i = 0;
	while (fd[i] != '>')
		i++;

	for (i++; i < (int)strlen(fd); i++)
		if (!strchr(x, fd[i]))		// если хотя бы один атрибут не входит в X,
			return 0;				// вовращаем "нет"
	return 1;
}
char * AddRightOfFDToX(char *fd, char *x)
{
	int i = 0;
	while(fd[i] != '>')
		i++;

	// добавляем в X только атрибуты, которых еще в нем нет
	for (i++; i < (int)strlen(fd); i++)
		if (!strchr(x, fd[i]))
			x = AddAttribute(x, fd[i]);
	return x;
}
// Функция добавления в последовательность атрибутов нового атрибута
char * AddAttribute(char *str, char a)
{
	// расширяем блок памяти str, учитывая 
	// кол-во уже добавленных атрибутов, нового ат-та и символа конца строки
	int new_length = (str == NULL) ? 2 : strlen(str) + 1 + 1;

	str = (char *)realloc(str, sizeof(char) * new_length);
	str[new_length - 2] = a;
	str[new_length - 1] = '\0';
	return str;
}
// Спасибо за внимание :3